$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/createNewObjective.feature");
formatter.feature({
  "line": 1,
  "name": "New Objective",
  "description": "",
  "id": "new-objective",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Creating a New Obective",
  "description": "",
  "id": "new-objective;creating-a-new-obective",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Test"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "The salesforce login page is opened",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Login with the below username and password",
  "rows": [
    {
      "cells": [
        "trial8685@trial.fairsail.com",
        "Fairsail123"
      ],
      "line": 7
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Once logged in, use the Workforce experience link to open the Fairsail employee portal",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "From the Navigation Bar expand the Performance Review section",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Click on the Objectives link for Objectives Detail view",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "Click New from the Objectives Details view and wait for the New Objective form",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "Enter the Description details on the form",
  "rows": [
    {
      "cells": [
        "testObjective",
        "testDescription",
        "testMeasure"
      ],
      "line": 13
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "Enter the Key Dates",
  "rows": [
    {
      "cells": [
        "Start Date",
        "Start Q3"
      ],
      "line": 15
    },
    {
      "cells": [
        "End Date",
        "End Q3"
      ],
      "line": 16
    },
    {
      "cells": [
        "Next Review Date",
        "Start Q4"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Click on Save",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "Goto Drafts and assert that the Objective is created",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "Click on Confirm draft \"testObjective\" and Click Yes on the Are you sure popup",
  "keyword": "Then "
});
formatter.match({
  "location": "loginStepDefinition.the_salesforce_login_page_is_opened()"
});
formatter.result({
  "duration": 5077501096,
  "status": "passed"
});
formatter.match({
  "location": "loginStepDefinition.login_with_the_below_username_and_password(DataTable)"
});
formatter.result({
  "duration": 1764707505,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.once_logged_in_use_the_Workforce_experience_link_to_open_the_Fairsail_employee_portal()"
});
formatter.result({
  "duration": 16989807907,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.from_the_Navigation_Bar_expand_the_Performance_Review_section()"
});
formatter.result({
  "duration": 220217039,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.click_on_the_Objectives_link_for_Objectives_Detail_view()"
});
formatter.result({
  "duration": 305182054,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.click_New_from_the_Objectives_Details_view_and_wait_for_the_New_Objective_form()"
});
formatter.result({
  "duration": 2505012281,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.enter_the_Description_details_on_the_form(DataTable)"
});
formatter.result({
  "duration": 5123631919,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.enter_the_Key_Dates(DataTable)"
});
formatter.result({
  "duration": 1485736886,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.click_on_Save()"
});
formatter.result({
  "duration": 1869741569,
  "status": "passed"
});
formatter.match({
  "location": "newObjectiveStepDefinition.goto_Drafts_and_assert_that_the_Objective_is_created()"
});
formatter.result({
  "duration": 2641774084,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testObjective",
      "offset": 24
    }
  ],
  "location": "newObjectiveStepDefinition.click_on_Confirm_draft_and_Click_Yes_on_the_Are_you_sure_popup(String)"
});
formatter.result({
  "duration": 4913909449,
  "status": "passed"
});
});