Feature: New Objective 

@Test
Scenario: Creating a New Obective 
	Given The salesforce login page is opened 
	Then Login with the below username and password 
		| trial8685@trial.fairsail.com | Fairsail123 |
	And Once logged in, use the Workforce experience link to open the Fairsail employee portal 
	Then From the Navigation Bar expand the Performance Review section 
	Then Click on the Objectives link for Objectives Detail view 
	Then Click New from the Objectives Details view and wait for the New Objective form 
	Then Enter the Description details on the form 
		| testObjective | testDescription | testMeasure | 
	Then Enter the Key Dates 
		| Start Date | Start Q3 |
		| End Date | End Q3 |
		| Next Review Date | Start Q4 |
	Then Click on Save 
	And Goto Drafts and assert that the Objective is created 
 	Then Click on Confirm draft "testObjective" and Click Yes on the Are you sure popup 
	#Then Assert the Newly created Objective "testObjective" is Active 
	
	
		