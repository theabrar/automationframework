package util;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//Add notes here
public class DriverFactory {

    protected static WebDriver driver;


    public DriverFactory() {
        initialize();
    }

    public void initialize() {
        if (driver == null)
            createNewDriverInstance();
    }

    private void createNewDriverInstance() {
    	System.setProperty("webdriver.chrome.driver",
				"src/webdriver/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().setSize(new Dimension(1400, 800));
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void destroyDriver() {
        driver.quit();
        driver.close();
        driver = null;
    }
}
