package runner;

import org.openqa.selenium.WebDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import util.DriverFactory;

@CucumberOptions(features="features/createNewObjective.feature",
glue={"stepDefinition"}, 
plugin = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber-json-report.json" })
public class RunCukesTest extends AbstractTestNGCucumberTests{
	
	private WebDriver driver;


    @Before
    public void beforeScenario() {
        driver = new DriverFactory().getDriver();
        System.out.println("starting scenario");
    }

    @After
    public void afterScenario() {
        new DriverFactory().destroyDriver();
        System.out.println("scenario finished");
    }
	
}
