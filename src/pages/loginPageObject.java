package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import global.GlobalObject;

public class loginPageObject extends GlobalObject{
	
	@FindBy(id="username")
	public WebElement username;
	
	@FindBy(id="password")
	public WebElement password;
	
	@FindBy(id="Login")
	public WebElement loginButton;
	
	@FindBy(xpath="//div[@class='loginError' and contains(text(), 'Please check your username')]")
	public WebElement loginError;
	
	public loginPageObject initElements(){
		return PageFactory.initElements(driver, loginPageObject.class);
	}
	
	public loginPageObject openPage() {
		openURL("https://login.salesforce.com/");
		return this;
	}
	
	public loginPageObject enterUsername(String username) {
		waitClearEnterText(initElements().username, username);
		return this;
	}
	
	public loginPageObject enterPassword(String password) {
		clearEnterText(initElements().password, password);
		return this;
	}
	
	public loginPageObject clickLoginButton() {
		elementToBeClickable(initElements().loginButton);
		initElements ().loginButton.click();
		return this;
	}

}
