package pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import global.GlobalObject;

public class objectivesPageObject extends GlobalObject{
	
	@FindBy(linkText="Workforce eXperience")
	public WebElement workforceExperienceTab;
	
	@FindBy(xpath="//div[@id='navigation-you']//b[text()='Performance ']")
	public WebElement performanceToggle;
	
	@FindBy(xpath="//div[@id='navigation-you']//a[text()='Objectives']")
	public WebElement objectives;
	
	@FindBy(xpath="//span[text()='Current Objectives']")
	public WebElement currentObjectivesSection;
	
	@FindBy(xpath="//h4[text()='New Objective: ']/../..//label[text()='Objective Name']/..//input")
	public WebElement objectiveNameTextbox;
	
	@FindBy(xpath="//h4[text()='New Objective: ']/../..//label[text()='Description']/..//textarea")
	public WebElement descriptionTextbox;
	
	@FindBy(xpath="//h4[text()='New Objective: ']/../..//label[text()='Measure']/..//textarea")
	public WebElement measureTextBox;
	
	@FindBy(xpath="//label[text()='Start Date']/..//input")
	public WebElement startDateDropdwon;
	
	@FindBy(xpath="//label[text()='End Date']/..//input")
	public WebElement endDateDropdown;
	
	@FindBy(xpath="//label[text()='Next Review Date']/..//input")
	public WebElement nextReviewDateDropdown;
	
	@FindBy(xpath="//button[contains(@ng-click, 'create') and text()='Save']")
	public WebElement saveButton;
	
	@FindBy(xpath="//button[contains(@ng-click, 'create') and text()='Save']")
	public List<WebElement> saveButtonNotDisplayed;
	
	@FindBy(xpath="//div[@id='tabs']//li[contains(text(), 'Draft')]")
	public WebElement draftTab;
	
	@FindBy(xpath="//h4[text()='Are you sure?']/../..//button[text()='OK']")
	public WebElement yesOnConfirmDialog;
	
	//@FindBy(xpath="//div[@class='row adminOption']//a[@class='logoutBtn' and contains(text(), 'Logout')]")
	@FindBy(xpath="//div[@class='row adminOption']//span[@class='glyphicon glyphicon-log-out']")
	public WebElement logOutButton;
	
	
	public objectivesPageObject initElements(){
		return PageFactory.initElements(driver, objectivesPageObject.class);
	}
	
	public objectivesPageObject clickWorkforceExperienceTab() {
		checkPageTitle("Salesforce - Enterprise Edition");
		elementToBeClickable(initElements().workforceExperienceTab);
		initElements().workforceExperienceTab.click();
		return this;
	}
	
	public objectivesPageObject clickToExpandPerformanceOption() {
		checkPageTitle("My Sage People");
		elementToBeClickable(initElements().performanceToggle);
		initElements().performanceToggle.click();
		return this;
	}
	
	public objectivesPageObject clickObjectives() {
		elementToBeClickable(initElements().objectives);
		initElements().objectives.click();
		return this;
	}
	
	public objectivesPageObject clickNewObjective() {
		visibilityOf(initElements().currentObjectivesSection);
		elementToBeClickable(initElements().currentObjectivesSection.findElement(By.xpath("..//button[contains(text(), 'New')]")));
		initElements().currentObjectivesSection.findElement(By.xpath("..//button[contains(text(), 'New')]")).click();
		return this;
	}
	
	public objectivesPageObject enterObjectivename(String objectiveName) {
		waitClearEnterText(initElements().objectiveNameTextbox, objectiveName);
		return this;
	}
	
	public objectivesPageObject enterDescription(String description) {
		waitClearEnterText(initElements().descriptionTextbox, description);
		return this;
	}
	
	public objectivesPageObject enterMeasure(String measure) {
		waitClearEnterText(initElements().measureTextBox, measure);
		return this;
	}
	
	public objectivesPageObject enterDate(String dateType, String value) {
		driver.findElement(By.xpath("//label[text()='"+dateType+"']/..//input")).click();
		elementToBeClickable(By.xpath("//label[text()='"+dateType+"']/..//ul[@class='quarters-list']//a[text()='"+value+"']"));
		driver.findElement(By.xpath("//label[text()='"+dateType+"']/..//ul[@class='quarters-list']//a[text()='"+value+"']")).click();
		return this;
	}
	
	public objectivesPageObject clickOnSave() {
		elementToBeClickable(initElements().saveButton);
		initElements().saveButton.click();
		invisibilityOfElementLocated(By.xpath("//button[contains(@ng-click, 'create') and text()='Save']"));
		assertTrue(driver.findElements(By.xpath("//button[contains(@ng-click, 'create') and text()='Save']")).isEmpty());
		return this;
	}
	
	public objectivesPageObject clickOnDraftTab() {
		elementToBeClickable(initElements().draftTab);
		initElements().draftTab.click();
		return this;
	}
	
	public objectivesPageObject clickOnConfirmDraft(String objectiveName) {
		WebElement ele = driver.findElement(By.xpath("//b[contains(text(), '"+objectiveName+"')]/../..//div[@label='fHCM2__Button_Confirm_Draft']"));
		elementToBeClickable(ele);
		ele.click();
		return this;
	}
	
	public objectivesPageObject clickYesOnConfirmDraftDialog() {
		elementToBeClickable(initElements().yesOnConfirmDialog);
		initElements().yesOnConfirmDialog.click();
		return this;
	}
	
	public objectivesPageObject assertObjectIsActive(String objectiveName) {
		elementToBeClickable(driver.findElement(By.xpath("//li[@class='ng-binding active' and contains "
				+ "(text(),  'Active')]/../..//b[@class='ng-binding' and contains(text(), '"+objectiveName+"')]")));
		visibilityOfAllElements(driver.findElements(By.xpath("//li[@class='ng-binding active' and contains "
				+ "(text(),  'Active')]/../..//b[@class='ng-binding' and contains(text(), '"+objectiveName+"')]")));
		return this;
	
	}
	
	public objectivesPageObject logOut() {
		elementToBeClickable(initElements().logOutButton);
		initElements().logOutButton.click();
		return this;
	}

}
