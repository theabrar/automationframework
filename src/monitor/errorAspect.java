package monitor;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.TestException;

import global.GlobalObject;

@Aspect
public class errorAspect extends GlobalObject {
	private static String fileSeperator = System.getProperty("file.separator");

	@After("execution(* global(*))")
	public void callCheckErrorMethod() {

		System.out.println("calling Check Error Method");
	}

	public void checkError() throws TestException {
		if (isElementPresent(By.xpath("//div[@class='loginError' and contains(text(), 'Please check your username')]"))) {
			try {
				takeScreenShot(getDriver(), "screenshot");
			} catch (TestException e) {
				System.out.println(e);
				throw new TestException(e);
			}
		}
	}
	
	public static String takeScreenShot(WebDriver driver,
			String screenShotName) {
		try {
			File file = new File("Screenshots" + fileSeperator + "Results");
			if (!file.exists()) {
				System.out.println("File created " + file);
				file.mkdir();
			}

			File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			File targetFile = new File("Screenshots" + fileSeperator + "Results", screenShotName);
			FileUtils.copyFile(screenshotFile, targetFile);

			return screenShotName;
		} catch (Exception e) {
			System.out.println("An exception occured while taking screenshot " + e.getCause());
			return null;
		}
	}

}
