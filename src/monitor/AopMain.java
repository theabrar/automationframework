package monitor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import global.GlobalObject;

public class AopMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		GlobalObject gloObj = context.getBean("gloObj", GlobalObject.class);
		
	}

}
