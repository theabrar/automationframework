package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class smokeTests {
	
	WebDriver driver;
	
	@Given("^Open browser and open google\\.com$")
	public void open_browser_and_open_google_com() throws Throwable {
		System.setProperty("webdriver.chrome.driver",
				"/Users/Abrar/Desktop/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.google.com");
	    throw new PendingException();
	}

	@Then("^Enter search term \"([^\"]*)\"$")
	public void enter_search_term(String arg1) throws Throwable {
		driver.findElement(By.id("email")).sendKeys("thethe");
		driver.findElement(By.id("pass")).sendKeys("PASS");
	    throw new PendingException();
	}

	@Then("^Click Search$")
	public void click_Search() throws Throwable {
		driver.findElement(By.id("loginbutton")).click();
	    throw new PendingException();
	}

}
