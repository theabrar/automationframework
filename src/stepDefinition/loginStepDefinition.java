package stepDefinition;

import java.util.List;

import org.testng.TestException;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pages.loginPageObject;

public class loginStepDefinition {
	
	private loginPageObject loginPage;
    
    public loginStepDefinition(loginPageObject loginPage) {
    	this.loginPage = loginPage;
    }
    
    
	@Given("^The salesforce login page is opened$")
	public void the_salesforce_login_page_is_opened() throws TestException {
		try{
			loginPage.openPage();
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^Login with the below username and password$")
	public void login_with_the_below_username_and_password(DataTable credentials) throws TestException {
		try{
			List<List<String>> data = credentials.raw();
			loginPage.enterUsername(data.get(0).get(0));
			loginPage.enterPassword(data.get(0).get(1));
			loginPage.clickLoginButton();
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

}
