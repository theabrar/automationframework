package stepDefinition;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.TestException;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pages.loginPageObject;
import pages.objectivesPageObject;
import util.DriverFactory;

public class newObjectiveStepDefinition {
	
    private objectivesPageObject objectivePage;
    
    public newObjectiveStepDefinition(objectivesPageObject objectivePage) {
		this.objectivePage = objectivePage;
	}
    
	@Then("^Once logged in, use the Workforce experience link to open the Fairsail employee portal$")
	public void once_logged_in_use_the_Workforce_experience_link_to_open_the_Fairsail_employee_portal() throws TestException {
		try{
			objectivePage.clickWorkforceExperienceTab();
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^From the Navigation Bar expand the Performance Review section$")
	public void from_the_Navigation_Bar_expand_the_Performance_Review_section() throws TestException {
		try{
			objectivePage.clickToExpandPerformanceOption();
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^Click on the Objectives link for Objectives Detail view$")
	public void click_on_the_Objectives_link_for_Objectives_Detail_view() throws TestException {
		try{
			objectivePage.clickObjectives();
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^Click New from the Objectives Details view and wait for the New Objective form$")
	public void click_New_from_the_Objectives_Details_view_and_wait_for_the_New_Objective_form() throws TestException {
		try{
			objectivePage.clickNewObjective();
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^Enter the Description details on the form$")
	public void enter_the_Description_details_on_the_form(DataTable details) throws TestException {
		try{
			List<List<String>> data = details.raw();
			objectivePage.enterObjectivename(data.get(0).get(0));
			objectivePage.enterDescription(data.get(0).get(1));
			objectivePage.enterMeasure(data.get(0).get(2));
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^Enter the Key Dates$")
	public void enter_the_Key_Dates(DataTable dates) throws TestException {
		try{
			List<List<String>> data = dates.raw();
			for (int i = 0; i < data.size(); i++) {
				int j = 0; int k = 1;
				objectivePage.enterDate(data.get(i).get(j), data.get(i).get(k));
			}
			
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	}

	@Then("^Click on Save$")
	public void click_on_Save() throws TestException {
		try{
			objectivePage.clickOnSave();
		}catch (TestException e) {
			System.out.println(e);
			throw new TestException(e);
		}
	    
	}

	@Then("^Goto Drafts and assert that the Objective is created$")
	public void goto_Drafts_and_assert_that_the_Objective_is_created() throws TestException {
	    try{
	    	objectivePage.clickOnDraftTab();
	    }catch (TestException e) {
	    	System.out.println(e);
	    	throw new TestException(e);
	    }
	}

	@Then("^Click on Confirm draft \"([^\"]*)\" and Click Yes on the Are you sure popup$")
	public void click_on_Confirm_draft_and_Click_Yes_on_the_Are_you_sure_popup(String newObjective) throws TestException {
	    try{
	    	objectivePage.clickOnConfirmDraft(newObjective).clickYesOnConfirmDraftDialog();
	    }catch (TestException e) {
	    	System.out.println(e);
	    	throw new TestException(e);
	    }
	}

	@Then("^Assert the Newly created Objective \"([^\"]*)\" is Active$")
	public void assert_the_Newly_created_Objective_is_Active(String newObjective) throws TestException {
	    try{
	    	objectivePage.assertObjectIsActive(newObjective);
	    }catch (TestException e) {
	    	System.out.println(e);
	    	throw new TestException(e);
	    }
	}
	
	@Then("^Click Logout$")
	public void click_logout() throws TestException {
		try{
			objectivePage.logOut();
		}catch (TestException e) {
	    	System.out.println(e);
	    	throw new TestException(e);
	    }
	}

}
